import scipy.stats as sp
import numpy as np
import sys
import os
from scipy.special import factorial
path_to_dmcalc = os.getenv('HOME')+'/dmcalc/install/python/'
sys.path.append(path_to_dmcalc)
import pydmcalc as dm
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.style.use('default')
import itertools

lz_radius = 72.8
lz_height = 72.8*2
lux_height = 48
lux_radius = 23.65
rho_xe = 2.94 #g/cm^3
rho_dm = .3
lz_depth = 2
c_light = 3e5 #km/s
livetime_s = 24*3600*85.3 #1000 days in s
lz_volume = np.pi*lz_radius**2 * lz_height
lux_volume = np.pi*lux_radius**2 * lux_height
lz_area = 2*np.pi*lux_radius**2 + 2 * np.pi*lux_radius*lux_height
lz_area = 2*np.pi*lz_radius**2 + 2 * np.pi*lz_radius*lz_height
xenon_A = 131.29276235756387 
avo = 6.022e23
n_T =  (rho_xe/xenon_A ) * avo
km2cm = 1e5
dama_floor = np.array([
       [1.03256286e+13, 1.28105300e-28],
       [9.94651899e+12, 7.83505087e-29],
       [1.01976798e+13, 1.38313689e-29],
       [9.94651899e+12, 9.03380780e-30],
       [9.94651899e+12, 3.06750556e-30],
       [1.07191879e+13, 1.04305948e-30],
       [9.94651899e+12, 3.53683162e-31],
       [1.01976798e+13, 1.05407890e-31],
       [9.94651899e+12, 4.07796422e-32],
       [1.11508967e+13, 8.58630300e-33],
       [1.48236523e+13, 9.58923450e-33],
       [1.95023993e+13, 1.11184824e-32],
       [2.56578857e+13, 1.30072002e-32],
       [3.37562106e+13, 1.51489931e-32],
       [4.44105866e+13, 1.77223815e-32],
       [5.84277728e+13, 2.06405861e-32],
       [7.68691633e+13, 2.40393084e-32],
       [1.01131157e+14, 2.79976714e-32],
       [1.33050894e+14, 3.27536893e-32],
       [1.75045367e+14, 3.83176210e-32],
       [2.30294434e+14, 4.54309648e-32],
       [3.02981607e+14, 5.41057911e-32],
       [3.98610823e+14, 6.47252764e-32],
       [5.24423215e+14, 7.77754344e-32],
       [6.89945411e+14, 9.38748696e-32],
       [9.07710905e+14, 1.10806286e-31],
       [1.19420910e+15, 1.33743074e-31],
       [1.57113389e+15, 1.59280677e-31],
       [2.06702638e+15, 1.90543112e-31],
       [2.71943599e+15, 2.28961144e-31],
       [3.57776378e+15, 2.75125166e-31],
       [4.70700311e+15, 3.26199838e-31],
       [6.19266102e+15, 3.91969497e-31],
       [8.14723287e+15, 4.70999888e-31],
       [1.07187206e+16, 5.58437052e-31],
       [1.41018397e+16, 6.74032950e-31],
       [1.85527630e+16, 8.06327161e-31],
       [2.44085184e+16, 9.60291493e-31],
       [2.90638547e+16, 1.65090154e-30],
       [3.09334853e+16, 7.09224146e-30],
       [3.18466793e+16, 2.38212616e-29],
       [3.21125091e+16, 2.96186494e-28],
       [3.21125091e+16, 1.00572620e-28]])

def projected_area(costheta, radius=72.8, height = 72.8*2):
    return np.pi*radius**2* np.abs(costheta) + 2* radius*height*(1-costheta**2)**.5

def projected_length(angle, radius, height):
    lz_volume = np.pi*radius**2 * height
    lz_area = 2*np.pi*radius**2 + 2 * np.pi*radius*height
    return lz_volume / projected_area(angle, radius, height)

def events_in_livetime_basic(mass, sigma, velocities):
    lengths = 4*lz_volume / lz_area
    total_xsection  = sigma * 131**4
    n_chi = rho_dm / mass
    flux = .25*km2cm*vel*vpdf* n_chi* areas *livetime_s
    counts = total_xsection*n_T * lengths
    dR_ss = flux * counts*np.exp(- counts)
    transits_ss = dR_ss.mean(axis = 1).sum()*dv
    dR_ms = flux * (1 - (counts+1)*np.exp(-counts ))
    transits_ms = dR_ms.mean(axis = 1).sum()*dv
    return transits_ss, transits_ms

def events_in_livetime(wimprate, velocities, angles, radius, height, shield):
    total_xsection = []
    areas = projected_area(angles, radius, height)[np.newaxis,:]
    lengths = np.pi*radius**2 * height / areas
    for velocity, angle in itertools.product(velocities[::-1], angles):
        energy = .5* wimprate.mass_GeV * (velocity/ 3e5)**2*1e6
        atten = shield.get_log_atten( angle, energy)
    #     atten=1
        total_xsection.append( wimprate.total_xsection(energy*np.exp(atten), 1, 100))
    total_xsection = np.array(total_xsection).reshape(len(velocities), len(angles))
    n_chi = rho_dm / wimprate.mass_GeV
    flux = km2cm*vel*vpdf* n_chi* areas *livetime_s
    counts = total_xsection*n_T * lengths
    dR_ss = flux * counts*np.exp(- counts)
    transits_ss = dR_ss.mean(axis = 1).sum()*dv
    dR_ms = flux * (1 - (counts+1)*np.exp(-counts ))
    transits_ms = dR_ms.mean(axis = 1).sum()*dv
    dR_ten =  flux * (sp.poisson.cdf(100,counts) -sp.poisson.cdf(1,counts))
    dR_hundred =  flux * (sp.poisson.cdf(100,counts) -sp.poisson.cdf(2,counts))
    transits_ten = dR_ten.mean(axis = 1).sum()*dv
    transits_hundred = dR_hundred.mean(axis = 1).sum()*dv
    return transits_ss, transits_ms, transits_ten, transits_hundred

def plot_limits(events):
    fig, ax = plt.subplots(figsize = (4,3), dpi = 150)
    ax.set_xscale('log')
    ax.set_yscale('log')
    # CS = plt.contour(masses,sigmas,events[:,:,0].T, colors= 'red', levels = [2.3])
    CS = ax.contour(masses,sigmas,events[:,:,0].T, colors= 'red', levels
            = [2.3],
            linestyles = ':')
    ax.clabel(CS, inline=True, manual = [( 1e14, 1e-30)],
            fmt = {CS.levels[0]:'1'},fontsize=10)

    CS = ax.contour(masses,sigmas,events[:,:,2].T, colors= 'green',
            linestyles = '-.',levels = [2.3])
    ax.clabel(CS, inline=True,
             manual = [( 1e14, 1e-30)],
            fmt = {CS.levels[0]:'2-100'},fontsize=10)
    CS = ax.contour(masses,sigmas,events[:,:,3].T, colors= 'green',
            linestyles = '--', levels = [2.3])

    ax.clabel(CS, inline=True,
             manual = [( 1e14, 1e-30)],
            fmt = {CS.levels[0]:'3-100'},fontsize=10)
    CS = ax.contour(masses,sigmas,events[:,:,1].T, colors= 'blue', levels = [2.3])
    ax.clabel(CS, inline=True, fmt = {CS.levels[0]:'2+'},fontsize=10)
    plt.plot(dama_floor[:,0], dama_floor[:,1], color = 'yellow', label = 'DAMA excluded')
    plt.fill_between(dama_floor[:,0], dama_floor[:,1], dama_floor[:,1].max(),
            color = "yellow", alpha = .5)

    ax.set_xlabel('DM Mass [GeV]', fontsize = 12)
    ax.set_ylabel('$\sigma_{\chi N}$ [cm$^2$]', fontsize = 12)

    plt.grid()
    plt.legend()
    # # plt.clabel(CS, inline=1, fmt = fmt,fontsize=10)
    plt.title('Projected Limits [LUX]')
    plt.savefig('ProjectedLimits_LUX.pdf', bbox_inches = 'tight')
    plt.close()

if __name__ == "__main__":

    target = dm.Target.Create("Xe")
    shm = dm.HaloSHM()
    wimprate = dm.RateBlob(mass = 1e18, xsection = 1e-45, number =1, target=target, halo=shm)
    energies = np.logspace(0, 3, 200, base =10)
    angles = np.r_[-1:1:30*1j]
    velocities = np.r_[:750:36*1j]
    masses = np.logspace(13, 17, 24, base = 10)
    sigmas = np.logspace(-33, -27,24,  base = 10)
    dv = (velocities[1]-velocities[0])

    
    xenon = dm.Target.Create("Xe")
    parameters = itertools.product(masses, sigmas, velocities, angles )
    # flux_dm = rho_dm*v_dm #g*cm^-2*s^-1
    vel = velocities[:, np.newaxis]
    vpdf = shm.velocity_pdf(velocities)[:, np.newaxis]
    events = []
    parameters = itertools.product(masses, sigmas)
    n=0
    xsection = dm.SICrossSection(masses[0], 1e-17)
    shield = dm.Overburden(xsection, 4850/5280 *1.609, True)
    for mass, sigma in parameters:
        if (n %10 ==0):
            print(n / (len(masses)*len(sigmas)))
        n+=1
        wimprate.mass_GeV = mass
        wimprate.dm_nucleon_cs_cm2 = sigma
        xsection.mass_GeV = mass
        xsection.scale = sigma
        events.append(events_in_livetime(wimprate, velocities, angles,
            lux_radius, lux_height, shield))
    events = np.array(events).reshape(len(masses), len(sigmas),-1)
    np.save('Event_counts.npy', events)
    plot_limits(events)
